package ourData;

import java.util.ArrayList;

public class Group {

    private ArrayList<Student> students;

    public Group(){
        students = new ArrayList<Student>();
    }

    public Group(ArrayList<Student> students){
        this.students = students;

    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public Student getStudent(int index){
        if(index < students.size()){
            return students.get(index);
        }
        return null;
    }

    public Group addStudent(Student student ){

        students.add(student);
        return this;
    }

    public float middleMarkGroup(){
        float sum = 0;
        for(Student it : students){
            sum += it.getStudentProgress().middleMark();
        }
        return sum / students.size();
    }

    public Group fiveMarksOnly(){
        Group group = new Group();
        for(Student it : students){
            if(it.getStudentProgress().middleMark() == 5)
            {
                group.addStudent(it);
            }
        }
        return group;
    }

    public Group bestsPeople(){
        Group group = new Group();
        for(Student it : students){
            if(it.getStudentProgress().isLoser())
            {
                group.addStudent(it);
            }
        }
        return group;
    }

    public int getSize(){
        return students.size();
    }

}
