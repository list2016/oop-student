package ourData;

public class Student {

    private StudentProgress studentProgress;

    public  Student()
    {
        studentProgress = new StudentProgress();
    }

    public Student(StudentProgress studentProgress)
    {
        this.studentProgress = studentProgress;
    }

    public StudentProgress getStudentProgress() {
        return studentProgress;
    }
}
