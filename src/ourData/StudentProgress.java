package ourData;

import java.util.ArrayList;

public class StudentProgress {

    private ArrayList<Integer> marks;

    public StudentProgress() {
        marks = new ArrayList<Integer>();
    }

    public ArrayList<Integer> getMarks() {
        return marks;
    }

    public int getMarks(int index){
        if(index < marks.size()){
            return marks.get(index);
        }
        return -1;
    }

    public StudentProgress addMarks(int mark){

        if(mark > 5){
            mark = 5;
        }
        else if(mark < 1){
            mark = 1;
        }
        //mark = (mark > 5) ? 5 : (mark < 1) ? 1 : mark;
        marks.add(mark);
        return this;
    }

    public float middleMark (){
        int sum = 0;
        for(int it : marks){
            sum += it;
        }
        return sum / marks.size();
    }

    public boolean isLoser(){
        for(int it : marks){
            if(it <= 2){
                return true;
            }
        }
        return false;
    }
}
