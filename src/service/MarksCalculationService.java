package service;

import ourData.*;

public class MarksCalculationService {

    public MarksCalculationService(){
        Group group = buildGroup(new Group());

        for (ourData.Student it : group.getStudents())
        {
            for (int _it : it.getStudentProgress().getMarks())
            {
                System.out.print(_it);
            }
            System.out.println();
            System.out.println("================================================");
        }

        System.out.println("Средная оценка группы: " + group.middleMarkGroup());

        for (ourData.Student it : group.getStudents())
        {
            System.out.println("Средний бал каждого студента: " + it.getStudentProgress().middleMark());
        }

        System.out.println("Число отличников в группе: " + group.fiveMarksOnly().getSize());

        System.out.println("Число двоечников в группе: " + group.bestsPeople().getSize());

    }

    public Group buildGroup(Group group)
    {
        for (int i = 0; i < (int)(Math.random() * 30); i++)
        {
            StudentProgress studentProgress = buildStudentProgress(new StudentProgress());
            group.addStudent(new ourData.Student(studentProgress));
        }
        return  group;
    }

    public StudentProgress buildStudentProgress(StudentProgress studentProgress)
    {
        for (int i = 0; i < 6; i++)
        {
            studentProgress.addMarks((int)(Math.random() * 32));
        }
        return studentProgress;
    }
}
